#!/usr/bin/env python
import csv
import redis
import argparse
import json
import re

parser = argparse.ArgumentParser(description='get results from redis.')
parser.add_argument('-c', dest='country', type=str, 
                   help='choose a country')
parser.add_argument('-p', dest='party', type=str,
                   help='choose a party')

args = parser.parse_args()

r = redis.StrictRedis(host='localhost', port=6379,  db=0)

parties = {}
party_list = []
countries = ['it' , 'uk' , 'fr' , 'nl']

# pstemer = PorterStemmer()
# whitelist = load_pickle("whitelist.pkl")
# whitelist = [word.lower() for word in whitelist if word not in countries+['ban']] + ['banq']
# whitelist = list(set(whitelist))
# whitelist.sort(key = lambda s: len(s))


fb_accounts = {}
for country in countries:
	with open('parties_'+country+'.csv', 'rb') as csvfile:
		spamreader = csv.reader(csvfile, delimiter='\t')
		for row in spamreader:
			if row[5] and '@' in row[5]:
				key = row[5][1:].lower()
				party_list += [key]
				party={}
				party['account'] = key
				party['type']= row[1]
				party['level']= row[4]
				party['id']= row[6]
				party['country']= country
				party['name']= re.sub(r'\W+','',row[2].lower())
				
				if country in parties:
					if party['name'] in parties[country]:
						parties[country][party['name']]  += [party]
					else:
						parties[country][party['name']] = [party]
						
				else:
					parties[country] = {}
					parties[country][party['name']] = [party]


filters={}

if args.country:
	filters["country"] = [args.country]

if args.party:
	filters["party"] = args.party.split(" ")

filters["source"] = "*"
filters["domain"] = "*"
filters["date"]  = "*"
filters["type"]  = None

# filters["country"] = ["it"]

final_data = {}
for country in parties:
	if "country" in filters:
		if country in filters["country"]:
			final_data[country] = {}
			final_data[country]["parties"] = {}
			country_keys = []
			for party in parties[country]:
				if "party" in filters :
					if party in filters["party"]:
						party_keys = []
						for account in parties[country][party]:
							if filters["type"]:
								if account['type'] == filters["type"]:
									keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
									party_keys += keys
									country_keys += keys
							else:
								keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
								party_keys += keys
								country_keys += keys
							# print account['account']
						if party_keys:
							r.zunionstore(party+":all",party_keys)
							final_data[country]["parties"][party] = r.zrevrange(party+":all" , 0 , -1, withscores=True)
				else:
					party_keys = []
					for account in parties[country][party]:
						if filters["type"]:
							if account['type'] == filters["type"]:
								keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
								party_keys += keys
								country_keys += keys
						else:
							keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
							party_keys += keys
							country_keys += keys
						# print account['account']
					if party_keys:
						r.zunionstore(party+":all",party_keys)
						final_data[country]["parties"][party] = r.zrevrange(party+":all" , 0 , -1, withscores=True)


			if country_keys:
				r.zunionstore(country+":all",country_keys)
				final_data[country]['top_terms'] = r.zrevrange(country+":all" , 0 , 30, withscores=True)
	else:
		final_data[country] = {}
		final_data[country]["parties"] = {}

		country_keys = []
		for party in parties[country]:
			if "party" in filters :
				if party in filters["party"]:
					party_keys = []
					for account in parties[country][party]:
						if filters["type"]:
							if account['type'] == filters["type"]:
								keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
								party_keys += keys
								country_keys += keys
						else:
							keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
							party_keys += keys
							country_keys += keys
						# print account['account']
					if party_keys:
						r.zunionstore(party+":all",party_keys)
						final_data[country]["parties"][party] = r.zrevrange(party+":all" , 0 , -1, withscores=True)
			else:
				party_keys = []
				for account in parties[country][party]:
					if filters["type"]:
						if account['type'] == filters["type"]:
							keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
							party_keys += keys
							country_keys += keys
					else:
						keys = r.keys(account['account'] +":"+filters["domain"]+":"+filters["source"]+":"+filters["date"]+":0")
						party_keys += keys
						country_keys += keys
					# print account['account']
				if party_keys:
					r.zunionstore(party+":all",party_keys)
					final_data[country]["parties"][party] = r.zrevrange(party+":all" , 0 , -1, withscores=True)


		if country_keys:
			r.zunionstore(country+":all",country_keys)
			final_data[country]['top_terms'] = r.zrevrange(country+":all" , 0 , 30, withscores=True)

	
# print final_data['uk']['top_terms']

final_data2 = []
for country in final_data:
	if "top_terms" in final_data[country]:
		for term in final_data[country]["top_terms"]:
			term_ob = {}
			term_ob["count"] = term[1]
			term_ob["name"] = term[0]
			term_ob["key"] = term[0]
			term_ob["pages"]  = []
			for party in final_data[country]["parties"]:
				for word in final_data[country]["parties"][party]:
					if word[0] == term[0]:
						party_ob = {}
						party_ob["name"] = party
						party_ob["count"] = word[1]
						party_ob["key"] = party
						party_ob["title"] = party
						party_ob["url"] = party
						term_ob["pages"] += [party_ob]
			final_data2 += [term_ob]
							

j = json.dumps(final_data2, indent=4)
f = open(filters["country"][0]+'_state_two.json', 'w')
print >> f, j
f.close()
print "done!"


# # import redis
# # r = redis.StrictRedis(host='localhost', port=6379,  db="infovis_web")

# # print r.keys("*")

# # # r.zunionstore("test",all_keys)
# # results = r.zrevrange("test" , 0 , -1, withscores=True)
# # # for result in results[0:10]:
# # 	print result
# # print sorted(all_keys)
# # prev_date = 0
# # yesterday = 0

# # for key in sorted(all_keys):
# # 	date = key[0:6]
# # 	if prev_date:
# # 		if date != prev_date:
# # 			yesterday = prev_date
# # 	prev_date = date
# # 	new_key = "_" + key

# # 	print key
# # 	print new_key

# 	if yesterday:
# 		yesterday_key = "_"+yesterday+key[6:]
# 		r.zunionstore(new_key,[key ,yesterday_key])
# 	else:
# 		r.zunionstore(new_key,[key])
# 		r.delete(key)
	# if yesterday:
	# 	
	# 	if r.keys(yesterday_key):
	# 		print yesterday_key
	# 		print r.zunionstore(new_key,[key ,yesterday_key])
	# else :
	# 	print new_key
	# 	print r.renamenx(key,new_key)



	# r.zunionstore("test",all_keys)
	# query_string = "immigr"
	# results = r.zrevrange("test" , 0 , -1,withscores=True)
	# for result in results:
	# 	if query_string in result[0]:
	# 		print result

