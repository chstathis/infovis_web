#!/usr/bin/env python
import redis
import json
from nltk.stem.snowball import SnowballStemmer
from collections import defaultdict  
from nltk.stem.snowball import PorterStemmer
from pickle_helpers import *

r = redis.StrictRedis(host='localhost', port=6379,  db=0)
countries = ['it' , 'uk' , 'fr' , 'nl']

bigcountries = {'it':"Italy" , 'uk':"United Kingdom" , 'fr':"France" , 'nl':"Netherlands"}
r.delete("test")

stemmers = {
	'it' : SnowballStemmer("italian"),
	'uk' : SnowballStemmer("english"),
	'nl' : SnowballStemmer("dutch"),
	'fr' : SnowballStemmer("french"),
}

pstemer = PorterStemmer()
whitelist = load_pickle("whitelist.pkl")
whitelist = [word.lower() for word in whitelist if word not in countries+['ban']] + ['banq']
whitelist = list(set(whitelist))
whitelist.sort(key = lambda s: len(s))

final_results = []
keys = []
stemmed_terms_dict = {}
for country in countries:
	keys = r.keys(country+":all:*:0")
	if keys:
		r.delete("test")
		r.zunionstore("test",keys)
		country_terms = r.zrevrange("test",0,-1,withscores=True)
		for term in country_terms:
			stem = 0
			for word in whitelist:
				if term[0].startswith(word):
					stem = word
					break
			if stem:
				# stem = stemmer.stem(term[0]) 
				if stem in stemmed_terms_dict:
					stemmed_terms_dict[stem]["count"] += term[1]
					stemmed_terms_dict[stem]["words"][term[0]] +=1
					if country in stemmed_terms_dict[stem]:
						stemmed_terms_dict[stem][country]["count"] += term[1]
					else:
						stemmed_terms_dict[stem][country] = {}
						stemmed_terms_dict[stem][country]["count"]  = term[1]
				else:
					stemmed_terms_dict[stem] = {}
					stemmed_terms_dict[stem]["words"] = defaultdict(int)
					stemmed_terms_dict[stem]["words"][term[0]] +=1
					stemmed_terms_dict[stem]["count"] = term[1]
					stemmed_terms_dict[stem][country] = {}
					stemmed_terms_dict[stem][country]["count"]  = term[1]




for stem in stemmed_terms_dict:
	word = {}
	word["count"] = stemmed_terms_dict[stem]["count"]
	word["key"] = stem
	word["name"] = [item[0] for item in list(sorted(stemmed_terms_dict[stem]["words"].iteritems(),key=lambda (k,v): v,reverse=True))][0]
	word["pages"] = []
	for country in countries:
		if country in  stemmed_terms_dict[stem]:
			word["pages"] += [{ "count" : stemmed_terms_dict[stem][country]["count"] , "name":bigcountries[country] , "key":bigcountries[country],"url":bigcountries[country],"title":bigcountries[country] } ]
	final_results +=[word]
# r.zunionstore("test" , keys)
# terms = r.zrevrange("test",0,-1,withscores = True)


# terms_dict = dict(terms)
# stemmed_terms_dict = {}
# for term in terms:
# 	stem = en_stemmer.stem(term[0])
# 	print en_stemmer.stem(term[0])
# 	if en_stemmer.stem(term[0]) in stemmed_terms_dict:
# 		stemmed_terms_dict[en_stemmer.stem(term[0])]["score"] += term[1]
# 	else:
		


# for term in terms:
# 	# print term
# 	# print "--------------------------------------"
# 	word = {}
# 	word["count"] = term[1]
# 	word["key"] = term[0]
# 	word["name"] = term[0]
# 	word["pages"] = []
# 	for country in countries:
# 		keys = r.keys(country+":all:*:0")
# 		r.delete("test")
# 		r.zunionstore("test" , keys)
# 		score =  r.zscore("test" , term[0])
# 		if not score:
# 			score = 0

# 		page = {}
# 		page["count"] = str(score)
# 		page["url"] = country
# 		page["name"] = country
# 		page["key"] = country
# 		page["title"] = country

# 		word["pages"]  +=  [page]
# 	final_results +=[word]
final_results.sort(key=lambda item:item['count'], reverse=True)
j = json.dumps(final_results, indent=4)
f = open('state_one.json', 'w')
print >> f, j
f.close()
print "done!"





